// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Interactable.h"


// Sets default values
ASnake::ASnake()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MoveSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoveSpeed);
	AddSnakeElement(5,0);
	DeathTimer();
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	TimeLeft = GetWorldTimerManager().GetTimerRemaining(GameOverTimer);
}

void ASnake::AddSnakeElement(int ElementNum,bool SpawnInvisible)
{
	if (SpawnInvisible == false)
	{
		for (int i = 0; i < ElementNum; ++i)
		{
			//FVector LastElemLocatiom = SnakeElements SnakeElements.Last()->GetActorLocation;

			FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
			FTransform NewTransform = FTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();

			}

		}

	}
	else
	{
		FVector NewLocation(0, 0, -400);
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();

		}
	}
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	

	
		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			
			break;
		}
	

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	MoveIsDone = false;
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableIterface = Cast<IInteractable>(Other);
		if (InteractableIterface)
		{
			InteractableIterface->Interact(this, bIsFirst);
		}
	}
}

void ASnake::SpeedUp(float SpeedBonus)
{
	SetActorTickInterval(SpeedBonus);
	
	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(
		Handle,
		FTimerDelegate::CreateWeakLambda(
			this,
			[this]

			{
				SetActorTickInterval(MoveSpeed);
			}),
		2.f,
		false
	);
}

void ASnake::DeathTimer()
{
	GetWorldTimerManager().SetTimer(GameOverTimer, this, &ASnake::Defeat, 50.f);	
}

void ASnake::Defeat_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("YouDied"));
	for (int i = SnakeElements.Num() -1; i > -1; i--)
	{
		auto Element = SnakeElements[i];
		if (IsValid(Element))
		{
			Element->Destroy();
		}
		
	}
	this->Destroy();
}


float ASnake::GetTimeLeft()
{
	return TimeLeft;
}

