// Fill out your copyright notice in the Description page of Project Settings.


#include "Grid.h"
#include "Marker.h"
#include "Snake.h"
#include "Food.h"

// Sets default values
AGrid::AGrid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 60.f;
}

// Called when the game starts or when spawned
void AGrid::BeginPlay()
{
	Super::BeginPlay();
	GenerateGrid(9,9,-10.f);
	GenerateFood();
}

// Called every frame
void AGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGrid::GenerateGrid(int Xnum, int Ynum,float Height)
{
	for (int i = 0; i < Xnum; ++i)
	{

		FVector NewLocationX(i * ElementSize, 0, Height);
		FTransform NewTransform = FTransform(NewLocationX);
		AMarker* NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
		NewGridElem->GridOwner = this;
		int32 ElemIndex = GridElements.Add(NewGridElem);
		
		for (int j = 0; j < Ynum; ++j)
		{

			FVector NewLocationY(i * ElementSize, j * ElementSize, Height);
			NewTransform = FTransform(NewLocationY);
			NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
			NewGridElem->GridOwner = this;
			ElemIndex = GridElements.Add(NewGridElem);


		}
	}	
	for (int i = 1; i < Xnum; ++i)
	{

		FVector NewLocationX(-i * ElementSize, 0, Height);
		FTransform NewTransform = FTransform(NewLocationX);
		AMarker* NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
		NewGridElem->GridOwner = this;
		int32 ElemIndex = GridElements.Add(NewGridElem);

		for (int j = 0; j < Ynum; ++j)
		{

			FVector NewLocationY(-i * ElementSize, j * ElementSize, Height);
			NewTransform = FTransform(NewLocationY);
			NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
			NewGridElem->GridOwner = this;
			ElemIndex = GridElements.Add(NewGridElem);


		}
	}
	for (int i = 0; i < Xnum; ++i)
	{

		FVector NewLocationX(i * ElementSize, 0, Height);
		FTransform NewTransform = FTransform(NewLocationX);
		AMarker* NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
		NewGridElem->GridOwner = this;
		int32 ElemIndex = GridElements.Add(NewGridElem);

		for (int j = 0; j < Ynum; ++j)
		{

			FVector NewLocationY(i * ElementSize, -j * ElementSize, Height);
			NewTransform = FTransform(NewLocationY);
			NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
			NewGridElem->GridOwner = this;
			ElemIndex = GridElements.Add(NewGridElem);


		}
	}
	for (int i = 0; i < Xnum; ++i)
	{

		FVector NewLocationX(-i * ElementSize, 0, Height);
		FTransform NewTransform = FTransform(NewLocationX);
		AMarker* NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
		NewGridElem->GridOwner = this;
		int32 ElemIndex = GridElements.Add(NewGridElem);

		for (int j = 0; j < Ynum; ++j)
		{

			FVector NewLocationY(-i * ElementSize, -j * ElementSize, Height);
			NewTransform = FTransform(NewLocationY);
			NewGridElem = GetWorld()->SpawnActor<AMarker>(MarkerClass, NewTransform);
			NewGridElem->GridOwner = this;
			ElemIndex = GridElements.Add(NewGridElem);


		}
	}
}

void AGrid::GenerateFood()
{
	int32 ElemNumber = GridElements.Num();
	int32 Random = RandRange(0, ElemNumber);
	//FVector Location = GridElements.Find(Random);
	//AFood* CurrentFood = GetWorld()->SpawnActor<AFood>(FoodClass, Location);
}

//int32 AGrid::RandRange(int32 min, int32 max)
//{
	//return int32();
//}

