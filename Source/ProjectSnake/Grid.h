// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Food.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Grid.generated.h"


class AMarker;

UCLASS()
class PROJECTSNAKE_API AGrid : public AActor, public FMath
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrid();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AMarker> MarkerClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	TArray<AMarker*> GridElements;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void GenerateGrid(int Xnum = 1, int Ynum = 1, float Heigt =0.f);

	UFUNCTION()

	void GenerateFood();

	//UFUNCTION()
	// 
	//static FORCEINLINE int32 RandRange(int32 min, int32 max);
};

