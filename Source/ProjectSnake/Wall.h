// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "obstacles.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Wall.generated.h"

UCLASS()
class PROJECTSNAKE_API AWall : public Aobstacles
{ 
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWall();

	UPROPERTY()
	FTimerHandle LifeTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void DestroyTimer();

	UFUNCTION()
	void Brake();

	UFUNCTION(BlueprintCallable)
	float GetTimeLeft();

};
