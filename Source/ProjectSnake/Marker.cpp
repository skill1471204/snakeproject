// Fill out your copyright notice in the Description page of Project Settings.


#include "Marker.h"
#include "Snake.h"
#include "Grid.h"

// Sets default values
AMarker::AMarker()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}


// Called when the game starts or when spawned
void AMarker::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMarker::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMarker::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnake>(Interactor);
	if (IsValid(Snake))
	{
		GridOwner->GridElements.Remove(this);
	}
	else
	{
		GridOwner->GridElements.Add(this);
	}
}



