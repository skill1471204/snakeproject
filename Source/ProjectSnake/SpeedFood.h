// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interactable.h"
#include "Food.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpeedFood.generated.h"

UCLASS()
class PROJECTSNAKE_API ASpeedFood : public AFood
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpeedFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
