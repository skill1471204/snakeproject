// Fill out your copyright notice in the Description page of Project Settings.


#include "obstacles.h"
#include "Snake.h"

// Sets default values
Aobstacles::Aobstacles()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void Aobstacles::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Aobstacles::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void Aobstacles::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Defeat();
			
		}
	}
}
